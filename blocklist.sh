#!/bin/bash
_tmpfile="$(mktemp)"
touch $_tmpfile

#this is the config file that is generated, change the path for your install
_unboundconf="/etc/unbound/unbound.conf.d/unbound-adhosts.conf"

# get the url, clean it, write to file
function getURL {
  # get the file at the url  |  remove comments and trailing spaces  |  get only the hostnames  |  remove localhost entries  |  remove 0.0.0.0 entires  |  remove blank lines  >>  output to list
  wget -O - $1 | sed -e 's/#.*//' -e 's/[\t\r\n\v\f]*$//' -e '/^$/ d' | grep -o "[^[:space:]]*$" | sed -r -e 's/^localhost$//' -e '/^$/ d' | sed -r -e 's/^0\.0\.0\.0$//' -e '/^$/ d' | sed -r -e 's/\"//' -e '/^$/ d' | sed '/^$/d' >> $_tmpfile
}

# my blocklists
getURL "https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt"
getURL "https://s3.amazonaws.com/lists.disconnect.me/simple_tracking.txt"
getURL "https://mirror1.malwaredomains.com/files/justdomains"
getURL "https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts"
#getURL "https://dbl.oisd.nl/"

echo "server:" > $_unboundconf

# Create unbound local zone file
sort -fu $_tmpfile | \
awk '{
  print "local-zone: \"" $1 "\" static"
}' >> $_unboundconf

rm -f $_tmpfile

chmod 660 $_unboundconf
chown root:unbound $_unboundconf

# Reload unbound blocklist, this won't break the server if someone goes wrong with the blocklist
/usr/sbin/unbound-checkconf 2>&1 && \
/usr/sbin/unbound-control reload 2>&1

exit 0
